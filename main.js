var templateString = document.getElementById('fb-page-template').innerHTML;
var template = Handlebars.compile(templateString);

function jsonpCallback(data) {
	var html = template(data);
	document.getElementById('results').innerHTML = html;
}

$('.sodas a').on('click', function() {
	var page = $(this).data('page');
	console.log(page);

	var script = document.createElement('script');
	script.src = "https://graph.facebook.com/"+page+"?callback=jsonpCallback";
	document.getElementsByTagName('head')[0].appendChild(script);
});